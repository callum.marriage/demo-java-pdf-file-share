package gov.uk.dvla.spring.mock.ukspf.backend;

public class MissingGrantAppDataException extends Exception {

  private String message;

  public MissingGrantAppDataException(String message){
    this.message = message;
  }
}
