package gov.uk.dvla.spring.mock.ukspf.backend;

import gov.uk.dvla.spring.mock.ukspf.backend.model.req.GrantApplication;
import gov.uk.dvla.spring.mock.ukspf.backend.pdf.PDFGenRequest;
import gov.uk.dvla.spring.mock.ukspf.backend.pdf.PDFGenRequestFactory;
import gov.uk.dvla.spring.mock.ukspf.backend.pdf.PDFResponse;
import gov.uk.dvla.spring.mock.ukspf.backend.pdf.PDFService;
import gov.uk.dvla.spring.mock.ukspf.backend.upload.FileUploader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Component
@Slf4j
public class MessageConsumer {

  private final PDFService pdfService;
  private final FileUploader fileUploader;

  public MessageConsumer(PDFService pdfService, FileUploader fileUploader) {
    this.pdfService = pdfService;
    this.fileUploader = fileUploader;
  }

  @KafkaListener(topics = "applicationSubmitted",
      containerFactory = "kafkaListenerContainerFactory",
      groupId = "grantApplication")
  public void listenGroupData(GrantApplication message) {
    PDFGenRequest pdfGenRequest = null;
    try {
      pdfGenRequest = PDFGenRequestFactory.buildPdfGenRequest(message);
    } catch (MissingGrantAppDataException e) {
      e.printStackTrace();
    }
    try {
      Optional<ByteArrayResource> response = pdfService.generatePDFFromData(pdfGenRequest);

      if (response.isEmpty()) {
        return;
      }
//
      long length = response.get().contentLength();

      InputStream stream = response.get().getInputStream();
//
      File targetFile = new File("output/new.pdf");
//
      FileUtils.copyInputStreamToFile(stream, targetFile);

      fileUploader.uploadToFileShare(stream, length);

    } catch (Exception e) {
      log.error(e.getMessage());
    }
    log.info("Received Message");
  }
}
