package gov.uk.dvla.spring.mock.ukspf.backend.config;

import com.azure.storage.file.share.ShareClient;
import com.azure.storage.file.share.ShareClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AzureConfig {

  @Value("azure.connection.string")
  public String connect;

  private static final String SHARE_NAME = "ukspf";

  @Bean
  public ShareClient shareClient(@Value("azure.connection.string") String connect){
    ShareClient shareClient = new ShareClientBuilder()
        .connectionString(connect)
        .shareName(SHARE_NAME)
        .buildClient();
    return shareClient;
  }
}
