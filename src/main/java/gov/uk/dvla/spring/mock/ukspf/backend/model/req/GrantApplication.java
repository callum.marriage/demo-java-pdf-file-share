package gov.uk.dvla.spring.mock.ukspf.backend.model.req;


import com.fasterxml.jackson.annotation.JsonProperty;

public class GrantApplication {

  @JsonProperty("personal-info")
  private PersonalInfo personalInfo;

  @JsonProperty("hobbies")
  private Hobbies hobbies;

  @JsonProperty("signin")
  private SignIn signIn;

  public GrantApplication(PersonalInfo personalInfo, Hobbies hobbies, SignIn signIn) {
    this.personalInfo = personalInfo;
    this.hobbies = hobbies;
    this.signIn = signIn;
  }

  public GrantApplication(){

  }

  public PersonalInfo getPersonalInfo() {
    return personalInfo;
  }

  public void setPersonalInfo(PersonalInfo personalInfo) {
    this.personalInfo = personalInfo;
  }

  public Hobbies getHobbies() {
    return hobbies;
  }

  public void setHobbies(Hobbies hobbies) {
    this.hobbies = hobbies;
  }

  public SignIn getSignIn() {
    return signIn;
  }

  public void setSignIn(SignIn signIn) {
    this.signIn = signIn;
  }
}
