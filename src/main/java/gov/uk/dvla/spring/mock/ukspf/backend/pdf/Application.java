package gov.uk.dvla.spring.mock.ukspf.backend.pdf;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Application {

  @JsonProperty("description")
  private String description;

  public Application() {
  }

  public Application(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
