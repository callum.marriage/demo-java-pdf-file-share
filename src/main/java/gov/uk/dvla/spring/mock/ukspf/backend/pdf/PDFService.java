package gov.uk.dvla.spring.mock.ukspf.backend.pdf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Service
public class PDFService {

  private final WebClient webClient;

  public PDFService(WebClient.Builder webClientBuilder, @Value("${baseUrl}") String baseUrl) {
    this.webClient = webClientBuilder.baseUrl(baseUrl).build();
  }

  public Optional<ByteArrayResource> generatePDFFromData(PDFGenRequest pdfGenRequest) {
    return webClient.post()
        .uri("/generatePdf")
        .accept(MediaType.APPLICATION_OCTET_STREAM)
        .contentType(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromValue(pdfGenRequest))
        .retrieve()
        .bodyToMono(ByteArrayResource.class)
        .blockOptional();
  }
}
