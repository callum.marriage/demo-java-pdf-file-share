package gov.uk.dvla.spring.mock.ukspf.backend.upload;

import com.azure.storage.file.share.ShareClient;
import com.azure.storage.file.share.ShareFileClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Service
@Slf4j
public class FileUploader {

  private final ShareClient fileShareClient;

  public FileUploader(ShareClient fileShareClient){
    this.fileShareClient = fileShareClient;
  }

  public void uploadToFileShare(InputStream pdf, long length){
    ShareFileClient shareFileClient = fileShareClient.getFileClient("ukspf/pdfs");
    shareFileClient.upload(pdf, length);
  }
}
