package gov.uk.dvla.spring.mock.ukspf.backend.model.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SignIn {

  @JsonProperty("email")
  private String email;

  @JsonProperty("password")
  private String password;

  public SignIn() {

  }

  public SignIn(String email, String password) {
    this.email = email;
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
