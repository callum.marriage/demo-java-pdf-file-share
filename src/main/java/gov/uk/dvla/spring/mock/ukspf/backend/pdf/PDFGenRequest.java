package gov.uk.dvla.spring.mock.ukspf.backend.pdf;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PDFGenRequest {

  @JsonProperty("applicant")
  private Applicant applicant;

  @JsonProperty("application")
  private Application application;

  @JsonProperty("msg_id")
  private String messageId;

  @JsonProperty("ref")
  private String ref;

  public PDFGenRequest(Applicant applicant, Application application, String messageId, String ref) {
    this.applicant = applicant;
    this.application = application;
    this.messageId = messageId;
    this.ref = ref;
  }

  public PDFGenRequest(){

  }

  public Applicant getApplicant() {
    return applicant;
  }

  public void setApplicant(Applicant applicant) {
    this.applicant = applicant;
  }

  public Application getApplication() {
    return application;
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  public String getMessageId() {
    return messageId;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }

  public String getRef() {
    return ref;
  }

  public void setRef(String ref) {
    this.ref = ref;
  }
}
