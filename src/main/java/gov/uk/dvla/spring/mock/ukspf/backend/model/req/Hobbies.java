package gov.uk.dvla.spring.mock.ukspf.backend.model.req;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Hobbies {

  @JsonProperty("description")
  private String description;

  public Hobbies() {
  }

  public Hobbies(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
