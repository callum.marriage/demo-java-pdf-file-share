package gov.uk.dvla.spring.mock.ukspf.backend.pdf;

import gov.uk.dvla.spring.mock.ukspf.backend.MissingGrantAppDataException;
import gov.uk.dvla.spring.mock.ukspf.backend.model.req.GrantApplication;
import gov.uk.dvla.spring.mock.ukspf.backend.model.req.Hobbies;
import gov.uk.dvla.spring.mock.ukspf.backend.model.req.PersonalInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PDFGenRequestFactory {


  public static PDFGenRequest buildPdfGenRequest(GrantApplication grantApplication) throws MissingGrantAppDataException {

    if(grantApplication.getPersonalInfo() == null || grantApplication.getHobbies() == null){
      throw new MissingGrantAppDataException("Data is missing");
    }

    PersonalInfo personalInfo = grantApplication.getPersonalInfo();
    Hobbies hobbies = grantApplication.getHobbies();

    Applicant applicant = new Applicant(personalInfo.getEmail(), personalInfo.getFirstName(), personalInfo.getLastName());
    Application application = new Application(hobbies.getDescription());
    return new PDFGenRequest(applicant, application, "", "");
  }
}
