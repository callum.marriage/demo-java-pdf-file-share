package gov.uk.dvla.spring.mock.ukspf.backend.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Collections;

public class WebClientConfig {

  @Bean
  public WebClient webClient(@Value("${baseUrl}") String baseUrl){
    return WebClient.builder()
        .baseUrl(baseUrl)
        .defaultCookie("cookieKey", "cookieValue")
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .defaultUriVariables(Collections.singletonMap("url", baseUrl))
        .build();
  }
}
