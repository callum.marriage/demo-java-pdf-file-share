package gov.uk.dvla.spring.mock.ukspf.backend.pdf;

public class PDFResponse {

  private String entity;

  public PDFResponse(String entity) {
    this.entity = entity;
  }

  public PDFResponse(){

  }

  public String getEntity() {
    return entity;
  }

  public void setEntity(String entity) {
    this.entity = entity;
  }
}
