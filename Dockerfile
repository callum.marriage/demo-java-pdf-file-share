FROM adoptopenjdk/openjdk11:x86_64-centos-jre-11.0.6_10
EXPOSE 5001

ADD src/main/resources/keystore/zscaler.pem /etc/pki/ca-trust/source/anchors/zscaler.pem

RUN update-ca-trust

COPY target/ms-ukspf-grant-submission-handler-*.jar /spring-mock-ukspf-backend.jar

ENTRYPOINT ["java", "-jar",  "/spring-mock-ukspf-backend.jar"]
